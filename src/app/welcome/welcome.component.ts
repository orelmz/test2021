import { environment } from 'src/environments/environment';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  email:any = "";
  afAuth: any;


  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.user.subscribe(
          user => {
            this.email =user?.email;
      }
    )

  }

}
