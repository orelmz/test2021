// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA69xVTi_SRMnFnzWg0P7V4Vwjh_d5V2F0",
    authDomain: "ex-orelmz.firebaseapp.com",
    projectId: "ex-orelmz",
    storageBucket: "ex-orelmz.appspot.com",
    messagingSenderId: "640261700408",
    appId: "1:640261700408:web:652b47e33276b4c9511fcd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
